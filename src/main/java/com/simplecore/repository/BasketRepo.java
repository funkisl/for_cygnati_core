package com.simplecore.repository;

import com.simplecore.domain.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BasketRepo extends JpaRepository<Basket, Integer> {
    List<Basket> findAll();
}
