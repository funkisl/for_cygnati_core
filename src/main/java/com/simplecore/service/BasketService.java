package com.simplecore.service;

import com.simplecore.domain.Basket;
import com.simplecore.repository.BasketRepo;
import com.simplecore.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BasketService {
    @Autowired
    private BasketRepo basketRepo;

    public List<Basket> getAll() {
        return basketRepo.findAll();
    }
    public Basket update(Basket basket) {
        return basketRepo.save(basket);
    }
    public void remove(Basket basket) {
        basketRepo.delete(basket);
    }
}
