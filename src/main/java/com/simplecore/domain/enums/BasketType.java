package com.simplecore.domain.enums;

public enum BasketType {
    CONFIRM(0),
    WAIT(1),
    SEND(2),
    RECEIVED(3),
    CLOSED(4),
    REJECTED(5);

    private int value;

    private BasketType(int value) {
        this.value = value;
    }
}
