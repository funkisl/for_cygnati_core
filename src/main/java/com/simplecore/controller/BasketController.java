package com.simplecore.controller;

import com.simplecore.domain.Basket;
import com.simplecore.response.Response;
import com.simplecore.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;


@RestController
public class BasketController {
    @Autowired
    BasketService basketService;

    @GetMapping(value = "/getAllItem", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Response getAll() {
        return Response.ok(basketService.getAll());
    }

    @RequestMapping(value = "/updateItem", method = RequestMethod.POST)
    @ResponseBody
    public Response updateItem(@RequestBody Basket basket) {

        basketService.update(basket);

        return Response.ok(basket);
    }

    @RequestMapping(value = "/removeItem", method = RequestMethod.POST)
    @ResponseBody
    public Response removeItem(@RequestBody Basket basket) {

        basketService.remove(basket);
        return Response.ok("");
    }

    @GetMapping(value = "/getAllItemByPage", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Response getAllByPage(@RequestParam(value = "page") int page,
                                 @RequestParam(value = "size") int size,
                                 @RequestParam(value = "sortOrder", required = false, defaultValue = "ASCENDING") SortOrder sortOrder,
                                 @RequestParam(value = "sortField") String sortField) {
        /*int page = 0;
        int size = 1;
        SortOrder sortOrder = SortOrder.ASCENDING;
        String sortField = "id";*/


       // basketService.getAllByPage(page, size, sortOrder, sortField);
        return Response.ok(basketService.getAllByPage(page, size, sortOrder, sortField));

    }

}


